import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlaceAppointmentRoutingModule } from './place-appointment-routing.module';
import { PlaceAppointmentComponent } from './place-appointment/place-appointment.component';


@NgModule({
  declarations: [
    PlaceAppointmentComponent
  ],
  imports: [
    CommonModule,
    PlaceAppointmentRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PlaceAppointmentModule { }
