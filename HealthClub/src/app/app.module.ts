import { query } from '@angular/animations';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaceAppointmentModule } from './place-appointment/place-appointment.module';
import { QueryModule } from './query/query.module';
import { ViewModule } from './view/view.module';
import { WelcomeModule } from './welcome/welcome.module';
import { HttpClientInterceptor } from './http-client.interceptor';
//import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WelcomeModule,
    PlaceAppointmentModule,
    QueryModule,ViewModule ,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {

     provide: HTTP_INTERCEPTORS,
      
    useClass:HttpClientInterceptor,
      
     multi:true
      
     }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


