import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaceAppointmentModule } from './place-appointment/place-appointment.module';
import { PlaceAppointmentComponent } from './place-appointment/place-appointment/place-appointment.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view/view/view.component';
import { WelcomeComponent } from './welcome/welcome/welcome.component';

const routes: Routes = [{path: 'welcome', component:WelcomeComponent },

{path: 'view', component:ViewComponent},

{path: 'query', component:QueryComponent},

{path: 'placeappointment', component:PlaceAppointmentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
