import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/app/health';
import { HealthService } from 'src/app/health.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  url ="http://localhost:3000/appointments";
  constructor(public api:HealthService) { }
  appointments:any;
  ngOnInit(): void {
    this.api.get(this.url).subscribe(res=>{
      this.appointments =res;
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }


}
